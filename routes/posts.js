var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://andrew:andrew@ds151909.mlab.com:51909/blog');
var bodyParser = require('body-parser');
var expJwt = require('express-jwt');
var config = require('../config');
var handleErrors =require('./helpers/helpers');


//Middlewares

//check for jsonwebtoken
var jwtCheck = expJwt({
  secret: config.secret
});

//Handling request without token
var UnauthorizedErrorHandler = function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    handleErrors(res,401,"invalid token");
  }
  else{
    next();
  }
};

//Validation id
var idValidate = function (err,req,res,next){
  let checkForHexRegExp = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/;
  if (checkForHexRegExp.test(req.params.id)===false){
    console.log('not valid');
    handleErrors(res,404,"wrong id");
  }
  else {
    console.log('valid');
    next();
  }
};

//end of middlewares

//List of endpoints

//get all
router.get('/api/v1/posts',function(req,res){
  db.posts.find(function(err,posts){
    if(err){
      handleErrors(res,424,"database problem");
    }
    res.json(posts);
  });
});

//get one
router.get('/api/v1/post/:id',idValidate,function(req,res){
  let checkForHexRegExp = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/;
  if (checkForHexRegExp.test(req.params.id)===false){
    handleErrors(res,404,"wrong id");
  }
  else{
    db.posts.findOne({'_id':mongojs.ObjectId(req.params.id)},function(err,post){
      if(err){
        handleErrors(res,424,"database problem");
      }
      else if(post==null){
        handleErrors(res,400,"wrong id");
      }
      else{
        res.status(200).json(post);//TODO add error
      }
    });
  }
});

//create one
router.post('/api/v1/post',jwtCheck,UnauthorizedErrorHandler,function(req,res){
  var post = req.body;
  if(!post.title || !post.text){
    handleErrors(res,400,"wrong input");
  }
  else if (post.title.length>150){
    handleErrors(res,400,"title to big");
    res.status(400).json({error:"title to big"});
  }
  else{
    newPost = {title:post.title,text:post.text,author_id:req.user._id,date:new Date()};
    db.posts.save(newPost,function(err,post){
      if(err){
        handleErrors(res,424,"database problem");
      }
      res.status(201).json(post);
    })
  }
});

//update
router.put('/api/v1/post/:id',
  jwtCheck,UnauthorizedErrorHandler,//middlewares
  function(req,res){
    let checkForHexRegExp = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/;
    if (checkForHexRegExp.test(req.params.id)===false){
      handleErrors(res,404,"wrong id");
    }
    else{
      var post = req.body;
      if(!post.title || !post.text){
        handleErrors(res,400,"wrong input");
      }
      else if (post.title.length>150){
        handleErrors(res,400,"title to big");
      }
      else{
        db.posts.findOne({'_id':mongojs.ObjectId(req.params.id)},function(err,doc){
          if (err){
            handleErrors(res,424,"database problem");
          }
          else if (doc.author_id !==req.user._id){
            handleErrors(res,401,"not created by this user");
          }
          else{
            doc.title = post.title;
            doc.text = post.text;
            db.posts.update({'_id':mongojs.ObjectId(req.params.id)},
            doc,function(err,task){
              if (err){
                handleErrors(res,424,"database problem");
              }
              res.status(202).json(task);
            });
          }
        });
      }}
  });

//delete
router.delete('/api/v1/post/:id',jwtCheck,UnauthorizedErrorHandler,function(req,res){
    let checkForHexRegExp = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/;
    if (checkForHexRegExp.test(req.params.id)===false){
      handleErrors(res,404,"wrong id");
    }
    else{
      db.posts.findOne({'_id':mongojs.ObjectId(req.params.id)},function(err,doc){
        if (err){
          handleErrors(res,424,"database problem");
        }
        else if (doc.author_id !==req.user._id){
          handleErrors(res,401,"not created by this user");
        }
        else{
          db.posts.remove({'_id':mongojs.ObjectId(req.params.id)},function(err,post){
            if (err){
              handleErrors(res,424,"database problem");
            }
            res.status(204).json({"answer":"No content"});
          });
        }
      });
    }
});

module.exports = router;
