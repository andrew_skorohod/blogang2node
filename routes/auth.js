var express = require('express');
var router = express.Router();
var jwt= require('jsonwebtoken');
var mongojs = require('mongojs');
var config = require('../config');

var db = mongojs(`mongodb://${config.user}:${config.password}@ds151909.mlab.com:51909/blog`);


//cipher decypher
const crypto = require('crypto');

var handleErrors =require('./helpers/helpers');

//creating json token with email and id
function createToken(user) {
  return jwt.sign(user, config.secret);
}

//cyphering password
function cyphering(password){
  var cipher = crypto.createCipher('aes192', config.secret);
  var password = cipher.update(password, 'utf8', 'hex');
  password+=cipher.final('hex');
  return password;
}

router.post('/login',function(req,res){
  var user = req.body;
  // validation
  if(!user.password || !user.email){
    handleErrors(res,400,"cannot be blank");
    //res.status(400).json({error:"cannot be blank"});
  }
  else if (user.password.length>256 || user.email.length >256) {
    handleErrors(res,400,"password or email is to big");
  }
  // cyphering password
  var password = cyphering(user.password);
  //Going to db check if any user has that password/email
  db.users.findOne({email:user.email,password:password},function(err,doc){
    if(err){
      handleErrors(res,424,"database problem");

    }
    else if(doc==null){
      handleErrors(res,401,"wrong password or email");
    }
    else{
      var tokenUser = {email:doc.email,_id:doc._id};
      res.status(200).json({id_token: createToken(tokenUser),user:tokenUser });
    }
  });
});


router.post('/create',function(req,res){
  var user = req.body;
  //checking if email in use
  db.users.findOne({email:user.email},function(err,doc){
    if(err){
      handleErrors(res,424,"database problem");
    }
    if (!doc){
      var password = cyphering(user.password);
      //saving new user
      db.users.save({email:user.email,password:password},function(err,doc){
        if(err){
          handleErrors(res,424,"database problem");

        };
        var user = {email:doc.email,_id:doc._id};
        res.status(201).json({id_token: createToken(user),user:user });//created
      });
    }
    else {
      handleErrors(res,400,"email already in use");
    }
  });
});

module.exports = router;
