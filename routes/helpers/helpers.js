function handleErrors(res,status,message){
	res.status(status).json({"error":message});
}

module.exports=handleErrors;