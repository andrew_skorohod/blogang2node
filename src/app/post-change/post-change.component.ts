import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Params,Router } from '@angular/router';
import { FormBuilder, FormGroup,Validators,FormControl } from '@angular/forms';
import { Post } from '../post.model';
import { PostsService } from '../posts.service';
import { AuthService } from '../auth.service';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-post-change',
  templateUrl: './post-change.component.html',
  styleUrls: ['./post-change.component.css']
})
export class PostChangeComponent implements OnInit {

  postForm: FormGroup;
  titleCtrl:FormControl;
  textCtrl:FormControl;
  user:any;
  post:Post;
  id:string;
  error:string;

  constructor(
  	private fb:FormBuilder,
    private postsService:PostsService,
    private router: Router,
  	private route: ActivatedRoute,
  	private location: Location,
    private authService:AuthService) 
  	{
  		
  		this.user = JSON.parse(localStorage.getItem('user')) || false;
	    this.authService.userObs.subscribe(res=>{this.user = res;});
	    this.route.params
		    .switchMap((params: Params) => this.postsService.getPost(params['id']))
		    .subscribe(
          post => this.post=post,
          error=>this.error=error);
  		this.titleCtrl = fb.control('');
	  	this.textCtrl = fb.control('', Validators.required);
	  	this.postForm = fb.group({
	  		title:this.titleCtrl,
	  		text:this.textCtrl
	  	});
    }

  ngOnInit() {
  	this.id = this.route.snapshot.params['id'];

  }

  updatePost(){
  	var updPost = this.postForm.value;
  	this.postsService.updatePost(updPost,this.id)
  	    .subscribe(post=>this.router.navigate(['/showpost',this.id]),
          error=>this.error=error);

  }

}
