import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  authenticated:any;
  history: string[] = [];

  constructor(private authService:AuthService ) {
    this.authenticated = localStorage.getItem('auth') || false;
    authService.authObs.subscribe(res=>this.authenticated = res);

   }

  ngOnInit() {

  }


  logout():void{
  	this.authService.logout();
  }

}
