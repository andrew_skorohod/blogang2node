import { Component, OnInit } from '@angular/core';
import { Post } from '../post.model';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit {
  posts:Post[];
  error:string;

  constructor(private postsService:PostsService) { 
  	this.postsService.getPosts()
  	.subscribe(
  		posts=>this.posts=posts,
  		error=>this.error=error)
  }

  ngOnInit() {
  }

}
