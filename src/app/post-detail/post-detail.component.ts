import { Component, OnInit,OnDestroy } from '@angular/core';
import { ActivatedRoute,Params,Router } from '@angular/router';
import { Post } from '../post.model';
import { PostsService } from '../posts.service';
import { AuthService } from '../auth.service';
import { Location } from '@angular/common';
import { DomSanitizer } from  '@angular/platform-browser';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {

  user:any;
  post:Post;
  id:string;
  text:any;
  error:string;


  constructor(private postsService:PostsService,
  	private route: ActivatedRoute,
    private router: Router,
  	private location: Location,
    private sanitizer:DomSanitizer,
    private authService:AuthService) {
      this.user = JSON.parse(localStorage.getItem('user')) || false;
      this.authService.userObs.subscribe(
          res=>{this.user = res;});
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
  	this.route.params
	    .switchMap((params: Params) => this.postsService.getPost(params['id']))
	    .subscribe(post => {this.post = post;
        this.text = this.sanitizer.bypassSecurityTrustHtml(this.post.text)},
        error=>this.error=error);
  }
  
  deletePost(){
    console.log(this.id)
    this.postsService.deletePost(this.id)
        .subscribe(res=>{
          console.log(res);
          this.router.navigate(['']);},
          error=>this.error=error);
  }
}
