import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators,FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {
  postForm: FormGroup;
  titleCtrl:FormControl;
  textCtrl:FormControl;
  error:string;

  constructor(
  	fb:FormBuilder,
    private postsService:PostsService,
    private router: Router
    ) {
  	    this.titleCtrl = fb.control('', Validators.required);
	  	this.textCtrl = fb.control('', Validators.required);
	  	this.postForm = fb.group({
	  		title:this.titleCtrl,
	  		text:this.textCtrl
	  	});
     }

  ngOnInit() {
  }

  createPost(){
  	var post = this.postForm.value;
  	this.postsService.createPost(post)
    .subscribe(post=>this.router.navigate(['/showpost',post._id]),
      error=>this.error=error);
  }
}