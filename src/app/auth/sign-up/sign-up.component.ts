import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators,FormControl } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  passwordForm: FormGroup;
  userForm: FormGroup;
  emailCtrl:FormControl;
  passwordCtrl:FormControl;
  passwordAgainCtrl:FormControl;
  error:string;

  static passwordMatch(group: FormGroup):any {
    var password = group.get('password').value;
    var passwordAgain = group.get('passwordAgain').value;
    return password === passwordAgain ? null : { matchingError: true };
  }


  constructor(
    fb:FormBuilder,
    private authService:AuthService,
    private router: Router) {
    	this.emailCtrl = fb.control('', Validators.required);
    	this.passwordCtrl = fb.control('', Validators.required);
    	this.passwordAgainCtrl = fb.control('', Validators.required);
      this.passwordForm = fb.group(
        { password: this.passwordCtrl, passwordAgain: this.passwordAgainCtrl },
        { validator: SignUpComponent.passwordMatch });
    	this.userForm = fb.group({
    		email:this.emailCtrl,
        passwordForm:this.passwordForm
    	});
   }

  register():void {
  	var user = this.userForm.value;
  	var newUser = {
      email:user.email,
      password:user.passwordForm.password
    };
    this.authService.signUp(newUser)
    .subscribe(
        user=>{
          this.authService.setAuth(user.id_token,user.user);
          this.router.navigate(['']);
        },
        error=> this.error = error);
  }

  ngOnInit() {
  }
  


}
