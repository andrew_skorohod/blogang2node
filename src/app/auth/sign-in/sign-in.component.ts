import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators,FormControl } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  userForm: FormGroup;
  emailCtrl:FormControl;
  passwordCtrl:FormControl;
  error:string;

  constructor(
    fb:FormBuilder,
    private authService:AuthService,
    private router: Router) 
  {
    this.emailCtrl = fb.control('', Validators.required);
    this.passwordCtrl = fb.control('', Validators.required);
    this.userForm = fb.group({
      email:this.emailCtrl,
      password:this.passwordCtrl
    });
  }

  ngOnInit() {

  }
  
  login():void {
    var user = this.userForm.value;
    var newUser = {
      email:user.email,
      password:user.password
    };
    this.authService.signIn(newUser)
       .subscribe(
        user=>{
          this.authService.setAuth(user.id_token,user.user);
          this.router.navigate(['']);
        },
        error=> this.error = error );
  }
}
