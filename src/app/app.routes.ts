import { Routes } from '@angular/router';
import { SignInComponent } from './auth/sign-in/sign-in.component';
import { SignUpComponent } from './auth/sign-up/sign-up.component';
import { PostsListComponent } from './posts-list/posts-list.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { PostCreateComponent } from './post-create/post-create.component';
import { PostChangeComponent } from './post-change/post-change.component';
import { ContactComponent } from './contact/contact.component';

export const ROUTES: Routes = [
  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: '', component: PostsListComponent },
  { path: 'showpost/:id',component: PostDetailComponent },
  { path: 'editpost/:id',component: PostChangeComponent },
  { path: 'create-post',component: PostCreateComponent },
  { path: 'contact',component: ContactComponent },
  { path: '**', component: PageNotFoundComponent },
];