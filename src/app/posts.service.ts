import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions,Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class PostsService {

  constructor(private http:Http) {
   }


  private extractData(res:Response){
    let body = res.json();
    return body || {};

  }

  private handleError(error: Response | any){
    let errMsg = error.json().error;
    let errStatus = error.status;
    console.log(`response returned with code ${errStatus} and message: ${errMsg}`);
    return Observable.throw(errMsg);
  }


  getPosts(){
  	return this.http.get('/api/v1/posts')
      .map(this.extractData)
      .catch(this.handleError);

  }

  getPost(id:string){
    return this.http.get(`/api/v1/post/${id}`)
      .map(this.extractData)
      .catch(this.handleError);
  }

  createPost(post){
    var headers = new Headers();
    headers.append("Content-Type","application/json");
    headers.append("Authorization","Bearer "+ localStorage.getItem('auth'));
    return this.http.post('/api/v1/post',JSON.stringify(post),{headers:headers})
      .map(this.extractData)//TODO 404 handler??
      .catch(this.handleError);
  }

//Not implemented
  updatePost(post,id){
    var headers = new Headers();
    headers.append("Content-Type","application/json");
    headers.append("Authorization","Bearer "+ localStorage.getItem('auth'));
    return this.http.put('/api/v1/post/'+id,JSON.stringify(post),{headers:headers})
      .map(this.extractData)
      .catch(this.handleError);
  }


  deletePost(id){
    let headers = new Headers({ 'Authorization': 'Bearer '+localStorage.getItem('auth') });
    let options = new RequestOptions({ headers: headers });
    return this.http.delete(`/api/v1/post/${id}`,options)
      .map(this.extractData)
      .catch(this.handleError);
  }
}
