import { Injectable } from '@angular/core';
import { Http,Headers,Response } from '@angular/http';
import { Subject }    from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {

  private auth = new Subject<any>();
  private user = new Subject<any>();
  authObs = this.auth.asObservable();
  userObs = this.user.asObservable();


  constructor(private http:Http) {
  }

  private extractData(res:Response){
    let body = res.json();
    return body || {};

  }

  private handleError(error: Response | any){
    let errMsg = error.json().error;
    let errStatus = error.status;
    console.log(`response returned with code ${errStatus} and message: ${errMsg}`);
    return Observable.throw(errMsg);
  }

  signIn(user){
    let headers = new Headers({"Content-Type":"application/json"});
    return this.http.post('/login',JSON.stringify(user),{headers:headers})
      .map(this.extractData)
      .catch(this.handleError);
  }
/*
  signUp(user){
  	let headers = new Headers({"Content-Type":"application/json"});
    return this.http.post('/create',JSON.stringify(user),{headers:headers})
      .map(res=>res.json());
  }
  */

  signUp(user){
    let headers = new Headers({"Content-Type":"application/json"});
    return this.http.post('/create',JSON.stringify(user),{headers:headers})
      .map(this.extractData)
      .catch(this.handleError);

  }

  setAuth(token:string,user):void{
    localStorage.setItem('auth',token);
    localStorage.setItem('user',JSON.stringify(user));
    this.auth.next(token);
  }
  
  logout():void{
    localStorage.removeItem('auth');
    localStorage.removeItem('user');
    this.auth.next(false);
  }

}
