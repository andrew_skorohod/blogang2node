var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var mongojs = require('mongojs');
var config = require('./config');
var expJwt = require('express-jwt');


var app = express();
var port = process.env.PORT || 8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
//set static
var distDir = __dirname + "/dist/";
app.use(express.static(distDir));

//route
app.use(require('./routes/posts'));
app.use(require('./routes/auth'));
// app.use('/user',user);
// app.use('/comments',comments);



app.listen(port,function(){
  console.log(`app is working on http://localhost:${port}/`);
});

//if not in router. render index.
app.get('*', function(req, res) {
    res.sendFile(distDir+'index.html');
});
