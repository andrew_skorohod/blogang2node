import { ExpBlogPage } from './app.po';

describe('exp-blog App', function() {
  let page: ExpBlogPage;

  beforeEach(() => {
    page = new ExpBlogPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
